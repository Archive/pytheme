#!/usr/bin/env python

import gtk
import cairo
import cairo.gtk
import random

def hand_square(ctx, x, y, width, height, thickness):
	intersection = thickness * 1/3;

	a1 = random.random() * (thickness - intersection) + intersection;
	a2 = random.random() * (thickness - intersection) + intersection;
	c1 = random.random() * (thickness - intersection) + intersection;
	c2 = random.random() * (thickness - intersection) + intersection;
	d1 = random.random() * (thickness - intersection) + intersection;
	d2 = random.random() * (thickness - intersection) + intersection;
	d3 = random.random() * (thickness - intersection) + intersection;
	d4 = random.random() * (thickness - intersection) + intersection;
	b1 = random.random() * (thickness - intersection) + intersection;
	b2 = random.random() * (thickness - intersection) + intersection;
	b3 = random.random() * (thickness - intersection) + intersection;
	b4 = random.random() * (thickness - intersection) + intersection;

	ctx.move_to(x, y + d1);
	ctx.curve_to(x + width * 1/3, y + d2,
		     x + width * 2/3, y + d3,
		     x + width, y + d4); 

	ctx.move_to(x, y + height - b1);
	ctx.curve_to(x + width * 1/3, y + height - b2,
		     x + width * 2/3, y + height - b3,
		     x + width, y + height - b4);

	ctx.move_to(x + a1, y);
	ctx.line_to(x + a2, y + height);

	ctx.move_to(x + width - c1, y);
	ctx.line_to(x + width - c2, y + height);

	ctx.stroke()

def expose_event(widget, event):
	button_thickness = 30
	padding = 5

	ctx = cairo.Context()
	cairo.gtk.set_target_drawable(ctx, widget.window)

	ctx.set_rgb_color(0,0,0)
	ctx.set_line_width(5)

	ctx.select_font('sans-serif')
	ctx.scale_font(48)

	border = button_thickness + padding;

	ctx.translate (50, 80);

   	(x, y, width, height, dx, dy) = ctx.text_extents('Cancel')
	ctx.move_to (0, 0);
	ctx.show_text('Cancel')

	hand_square(ctx, x - border, y - border,
		    width + border * 2, height + border * 2,
		    button_thickness)

	ctx.translate (width + border * 2 + 20, 0);

   	(x, y, width, height, dx, dy) = ctx.text_extents('OK')
	ctx.move_to (0, 0);
	ctx.show_text('OK')

	hand_square(ctx, x - border, y - border,
		    width + border * 2, height + border * 2,
		    button_thickness)

win = gtk.Window()
win.set_title('Hand Button Demo')

drawingarea = gtk.DrawingArea()
drawingarea.set_size_request(400,100)
win.add(drawingarea)

win.show_all()

win.connect('destroy', lambda x: gtk.main_quit())
drawingarea.connect('expose_event', expose_event)

gtk.main()
