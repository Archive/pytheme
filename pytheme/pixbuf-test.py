#!/usr/bin/python


import gtk
import Numeric
import random

def draw_check_mark (pixels):
    lx = pixels.shape[1] - 2;
    ly = pixels.shape[0] - 2;
    x = 2
    y = 2

    while x + 1 < lx and y + 1 < ly:
	pixels[int(y),int(x), 0] = 255
	pixels[int(y),int(x), 1] = 0
	pixels[int(y),int(x), 2] = 0
	pixels[int(y),int(x) + 1, 0] = 255
	pixels[int(y),int(x) + 1, 1] = 0
	pixels[int(y),int(x) + 1, 2] = 0
	pixels[int(y) + 1,int(x), 0] = 255
	pixels[int(y) + 1,int(x), 1] = 0
	pixels[int(y) + 1,int(x), 2] = 0
	x = x + random.random()
	y = y + random.random() * 1.8

    while x + 1 < lx and y > 6:
        pixels[int(y),int(x), 0] = 255
	pixels[int(y),int(x), 1] = 0
	pixels[int(y),int(x), 2] = 0
	pixels[int(y),int(x) + 1, 0] = 255
	pixels[int(y),int(x) + 1, 1] = 0
	pixels[int(y),int(x) + 1, 2] = 0
	pixels[int(y) + 1,int(x), 0] = 255
	pixels[int(y) + 1,int(x), 1] = 0
	pixels[int(y) + 1,int(x), 2] = 0
	x = x + random.random()
	y = y - (random.random() * 1.4)

def frob_button_line (pixels):
    g = 0
    for pixel in pixels:
        r = random.randint(0, 20)
	if g == 0 and r == 5:
		g = random.randint(0, 50)
	if g > 0:
		pixel += Numeric.array (150 + 105 / g).astype (Numeric.UnsignedInt8)
		g = g - 1

def frob_button_line (pixels):
    g = 0
    for pixel in pixels:
        r = random.randint(0, 20)
	if g == 0 and r == 5:
		g = random.randint(0, 50)
	if g > 0:
		pixel += Numeric.array (150 + 105 / g).astype (Numeric.UnsignedInt8)
		g = g - 1

def frob_button_pixels (pixels):
    top_row = pixels[0,:,:]
    frob_button_line (top_row)
    
    bottom_row = pixels[-1,:,:]
    frob_button_line (bottom_row)

    left_col = pixels[1:-1,0,:]
    frob_button_line (left_col)

    right_col = pixels[1:-1,-1,:]
    frob_button_line (right_col)


def frob_line (pixels):
    g = 0
    for pixel in pixels:
        r = random.randint(0, 20)
	if g == 0 and r == 5:
		g = random.randint(0, 50)
	if g > 0:
		pixel += Numeric.array (100 + 155 / g).astype (Numeric.UnsignedInt8)
		g = g - 1

def frob_pixels (pixels):
    top_row = pixels[0,:,:]
    frob_line (top_row)
    
    bottom_row = pixels[-1,:,:]
    frob_line (bottom_row)

    left_col = pixels[1:-1,0,:]
    frob_line (left_col)

    right_col = pixels[1:-1,-1,:]
    frob_line (right_col)

def get_pixbuf ():
    pixbuf = gtk.gdk.pixbuf_new_from_file ("entry.png")
    pixels = pixbuf.get_pixels_array ()
    frob_pixels (pixels)
    return pixbuf

def get_button_pixbuf ():
    pixbuf = gtk.gdk.pixbuf_new_from_file ("button.png")
    pixels = pixbuf.get_pixels_array ()
    frob_button_pixels (pixels)
    return pixbuf

def get_checkbox_pixbuf ():
    pixbuf = gtk.gdk.pixbuf_new_from_file ("checkbox.png")
    pixels = pixbuf.get_pixels_array ()
    frob_button_pixels (pixels)
    draw_check_mark (pixels);
    return pixbuf

window = gtk.Window ()
window.set_border_width (12)
window.connect ('delete-event', gtk.main_quit)
vbox = gtk.VBox (False, 36)
window.add (vbox)

table = gtk.Table (2, 3)
table.set_row_spacings (12);
table.set_col_spacings (24);
vbox.pack_start (table);

l = gtk.Label("Name:")
table.attach (l, 0, 1, 0, 1)
p = get_pixbuf ()
i = gtk.Image ()
i.set_from_pixbuf (p)
table.attach (i, 1, 2, 0, 1)

l = gtk.Label("Surname:")
table.attach (l, 0, 1, 1, 2)
p = get_pixbuf ()
i = gtk.Image ()
i.set_from_pixbuf (p)
table.attach (i, 1, 2, 1, 2)

l = gtk.Label("Address:")
table.attach (l, 0, 1, 2, 3)
p = get_pixbuf ()
i = gtk.Image ()
i.set_from_pixbuf (p)
table.attach (i, 1, 2, 2, 3)

hbox = gtk.HBox(False, 12)
p = get_checkbox_pixbuf ()
i = gtk.Image ()
i.set_from_pixbuf (p)
hbox.pack_start (i, False, False)
l = gtk.Label("Checkbox 1")
hbox.pack_start (l, False, False);
vbox.pack_start (hbox);

hbox = gtk.HBox(False, 12)
p = get_checkbox_pixbuf ()
i = gtk.Image ()
i.set_from_pixbuf (p)
hbox.pack_start (i, False, False)
l = gtk.Label("Checkbox 2")
hbox.pack_start (l, False, False);
vbox.pack_start (hbox);

hbox = gtk.HBox(False, 12)

p = get_button_pixbuf ()
i = gtk.Image ()
i.set_from_pixbuf (p)
hbox.pack_start (i)

p = get_button_pixbuf ()
i = gtk.Image ()
i.set_from_pixbuf (p)
hbox.pack_start (i)

vbox.pack_start (hbox);

window.show_all ()
gtk.main ()
