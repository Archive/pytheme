#!/bin/sh

export GTK_DATA_PREFIX=`pwd`
export GTK_EXE_PREFIX=`pwd`

mkdir -p lib/gtk-2.0/2.4.0/engines/

if [ -f .libs/libpixmap.so ]; then
    cp .libs/libpixmap.so lib/gtk-2.0/2.4.0/engines/libpixmap.so
else
    exit 1
fi

exec ./theme-test "$@"
