#include <gtk/gtk.h>

static void
create_window (void)
{
  GtkWidget *window;
  GtkWidget *box;
  GtkWidget *widget;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (window, "delete-event", gtk_main_quit, NULL);
  box = gtk_vbox_new (FALSE, 12);

  gtk_container_set_border_width (GTK_CONTAINER (box), 12);
  gtk_container_add (GTK_CONTAINER (window), box);

#if 1
  widget = gtk_button_new_with_label ("Foo!");
  gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);

  widget = gtk_button_new_with_label ("Bar!");
  gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);

  widget = gtk_entry_new ();
  gtk_entry_set_text (GTK_ENTRY (widget), "Entry 1!");
  gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);

  widget = gtk_entry_new ();
  gtk_entry_set_text (GTK_ENTRY (widget), "Entry 2!");
  gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);

  widget = gtk_spin_button_new (NULL, 1.0, 3);
  gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);

#endif
  widget = gtk_spin_button_new (NULL, 1.0, 3);
  gtk_box_pack_start (GTK_BOX (box), widget, FALSE, FALSE, 0);

  gtk_widget_show_all (window);
}

int
main (int argc, char *argv[])
{
  GtkSettings *settings;
  char *font_name;

  gtk_init (&argc, &argv);
  if (argc == 2)
    font_name = argv[1];
  else
    font_name = "Sans 10";
  settings = gtk_settings_get_default ();
  g_object_set (G_OBJECT (settings),
		"gtk-theme-name", "aha",
		"gtk-font-name", font_name,
		NULL);

  create_window ();

  gtk_main ();

  return 0;
}
