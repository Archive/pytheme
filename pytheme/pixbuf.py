import gtk
import Numeric
import random

def frob_pixels (pixels):
    i = random.randint (-25, 25)
    for pixel in pixels:
        pixel += Numeric.array(i).astype (Numeric.UnsignedInt8)

def frob_line (pixels):
    g = 0
    for pixel in pixels:
        r = random.randint(0, 20)
	if g == 0 and r == 5:
		g = random.randint(0, 50)
	if g > 0:
		pixel += Numeric.array (100 + 155 / g).astype (Numeric.UnsignedInt8)
		g = g - 1

def frob_button_line (pixels):
    g = 0
    for pixel in pixels:
        r = random.randint(0, 20)
	if g == 0 and r == 5:
		g = random.randint(0, 50)
	if g > 0:
		pixel += Numeric.array (150 + 105 / g).astype (Numeric.UnsignedInt8)
		g = g - 1

def draw_shadow (pixbuf, detail, component):
    pixels = pixbuf.get_pixels_array ()
    frob_line (pixels)

def draw_box (pixbuf, detail, component):
    pixels = pixbuf.get_pixels_array ()
    if component != 'center':
        frob_button_line (pixels)

def update_image(pixbuf, draw_type, detail, component, seed):
    try:
        # Keep the seed consistent across widgets
#        print '(%d,%d) ' % (pixbuf.get_width(), pixbuf.get_height()), draw_type, detail, component
        random.seed (seed)
        if draw_type == 'shadow':
            draw_shadow (pixbuf, detail, component)
        elif draw_type == 'box':
            draw_box (pixbuf, detail, component)
    except Exception, e:
        print e

def initialize_engine():
    return {"update_image": update_image
            }
